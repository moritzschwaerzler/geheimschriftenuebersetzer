﻿using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Geheimschriften
{
    /// <summary>
    /// Interaktionslogik für drehscheibe.xaml
    /// </summary>
    public partial class drehscheibe : Window
    {
        private mvvm mvvm;
        public drehscheibe(ref mvvm mvvm)
        {
            InitializeComponent();
            this.mvvm = mvvm;
            this.mvvm.requestDreh();
            DataContext = this.mvvm;
            GetIcon();
        }

        private void GetIcon()
        {
            Bitmap icon = Properties.Resources.logo;

            Icon = MainWindow.GetSourceFromBitmap(icon);
        }

        private void btclose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btpic_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetImage(MainWindow.CaptureScreen(savepanel, 300, 300));
        }

        ScrollViewer sv1, sv2;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            sv1 = VisualTreeHelper.GetChild(VisualTreeHelper.GetChild(upper, 0), 0) as ScrollViewer;
            sv2 = VisualTreeHelper.GetChild(VisualTreeHelper.GetChild(lower, 0), 0) as ScrollViewer;

            sv1.ScrollChanged += new ScrollChangedEventHandler(sv1_ScrollChanged);
            sv2.ScrollChanged += new ScrollChangedEventHandler(sv2_ScrollChanged);

            sv1.ScrollToHorizontalOffset(20);
        }

        void sv1_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            sv2.ScrollToHorizontalOffset(sv1.HorizontalOffset);
        }

        void sv2_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            sv1.ScrollToHorizontalOffset(sv2.HorizontalOffset);
        }
    }
}
