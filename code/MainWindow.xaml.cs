﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Geheimschriften
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 

    public class mvvm : INotifyPropertyChanged
    {
        private List<string> drehlist;

        private bool isinitalised = false;
        public mvvm()
        {
            Radios1 = true;
            Radios2 = false;
            Radios3 = false;
            Radios4 = false;
            MITUMLAUT = false;
            MITZAHLEN = false;
            SCROLLBAR = true;

            IN = "";
            OUT = "";

            SIZE = 10;

            VERSCHIEB = 0;
            TOGGLECHECK = false;
            TOGGLETEXT = "zurück verschieben";

            HELP = "übersetzbare Zeichen: a-z, ß, äöü, 0-9 mit .,!?";

            CANVASVISI = Visibility.Collapsed;
            TEXTBOXVISI = Visibility.Visible;
            VERSCHIEBVISI = Visibility.Collapsed;
            MORSEVISI = Visibility.Visible;
            SCROLLBARVISI = ScrollBarVisibility.Auto;

            alphabet = new char[0];
            alphabet = alphabet.Concat(nuralphabet).ToArray();
            alphabet = alphabet.Concat(umlaute).ToArray();
            alphabet = alphabet.Concat(zahlen).ToArray();
            alphabet = alphabet.Concat(sonderzeichen).ToArray();
            timeout();
        }

        private void setDrehList()
        {
            drehlist = new List<string>(nuralphabet.Select(e => e.ToString()));

            if (MITUMLAUT == true)
            {
                drehlist.AddRange(umlaute.Select(e => e.ToString()));
            }

            if (MITZAHLEN == true)
            {
                drehlist.AddRange(zahlen.Select(e => e.ToString()));
            }

            MAXVERSCHIEB = drehlist.Count - 1;
        }

        public void requestDreh()
        {
            setDrehList();

            LOWER = new ObservableCollection<string>();
            UPPER = new ObservableCollection<string>();

            LOWER.Add("...");
            UPPER.Add("...");


            int drehlength = drehlist.Count;
            int doubledreh = drehlength * 3 + 1;

            for (int i = 0; i < doubledreh; i++)
            {
                int a = i % drehlength;
                int b = (i + (int)VERSCHIEB) % drehlength;

                if (a == 0 && i != 0)
                {
                    UPPER.Add("|");
                    LOWER.Add("|");
                }

                UPPER.Add(drehlist[a].ToString());
                LOWER.Add(drehlist[b].ToString());

            }

            LOWER.Add("...");
            UPPER.Add("...");

            OnPropChanged("UPPER");
            OnPropChanged("LOWER");

        }

        private async void timeout()
        {
            await Task.Delay(250);

            isinitalised = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }

        //abc AÖÜ zahlen .!?,
        private readonly string[] morsealphabet = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".-.-", "---.", "..--", "...--..", "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", ".-.-.-", "-.-.--", "..--..", "..--." };
        private readonly char[] sonderzeichen = { '.', '!', '?', ',' };
        private readonly char[] nuralphabet = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        private readonly char[] zahlen = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private readonly char[] umlaute = { 'ä', 'ö', 'ü', 'ß' };
        private readonly char[] alphabet;

        #region -------------------------Props-----------------------

        private Visibility falschzeichen;

        public Visibility Falschzeichen
        {
            get { return falschzeichen; }
            set {
                falschzeichen = value;
                OnPropChanged("Falschzeichen");
            }
        }


        private Visibility morsevisi;

        public Visibility MORSEVISI
        {
            get { return morsevisi; }
            set
            {
                morsevisi = value;
                OnPropChanged("MORSEVISI");
            }
        }


        private ScrollBarVisibility scrollbarvisi;

        public ScrollBarVisibility SCROLLBARVISI
        {
            get { return scrollbarvisi; }
            set
            {
                scrollbarvisi = value;
                OnPropChanged("SCROLLBARVISI");
            }
        }

        private bool? scrollbar;

        public bool? SCROLLBAR
        {
            get { return scrollbar; }
            set
            {
                scrollbar = value;
                if (scrollbar == true)
                {
                    SCROLLBARVISI = ScrollBarVisibility.Auto;
                }
                else
                {
                    SCROLLBARVISI = ScrollBarVisibility.Disabled;
                }
                OnPropChanged("SCROLLBAR");
            }
        }


        private double maxverschieb;

        public double MAXVERSCHIEB
        {
            get { return maxverschieb; }
            set
            {
                maxverschieb = value;
                OnPropChanged("MAXVERSCHIEB");
            }
        }


        private bool? mitumlaut;

        public bool? MITUMLAUT
        {
            get { return mitumlaut; }
            set
            {
                mitumlaut = value;
                OnPropChanged("MITUMLAUT");
                VERSCHIEB = 0;
                OnChange();
            }
        }

        private ObservableCollection<string> upper;

        public ObservableCollection<string> UPPER
        {
            get { return upper; }
            set { upper = value; }
        }


        private bool? mitzahlen;

        public bool? MITZAHLEN
        {
            get { return mitzahlen; }
            set
            {
                mitzahlen = value;
                OnPropChanged("MITZAHLEN");
                VERSCHIEB = 0;
                OnChange();
            }
        }

        private ObservableCollection<string> lower;

        public ObservableCollection<string> LOWER
        {
            get { return lower; }
            set { lower = value; }
        }


        private string verschiebstatus;

        public string VERSCHIEBSTATUS
        {
            get { return verschiebstatus; }
            set
            {
                verschiebstatus = value;
                OnPropChanged("VERSCHIEBSTATUS");
            }
        }


        private int verschiebindex;

        public int VERSCHIEBINDEX
        {
            get { return verschiebindex; }
            set { verschiebindex = value; }
        }


        private string toggletext;

        public string TOGGLETEXT
        {
            get { return toggletext; }
            set
            {
                toggletext = value;
                OnPropChanged("TOGGLETEXT");
            }
        }


        private bool? togglecheck;

        public bool? TOGGLECHECK
        {
            get { return togglecheck; }
            set
            {
                togglecheck = value;
                OnPropChanged("TOGGLECHECK");
                OnChange();
            }
        }


        private double verschieb;

        public double VERSCHIEB
        {
            get { return verschieb; }
            set
            {
                verschieb = value;
                OnPropChanged("VERSCHIEB");
                OnChange();
            }
        }


        private char verschiebchar;

        public char VERSCHIEBCHAR
        {
            get { return verschiebchar; }
            set
            {
                verschiebchar = value;
                OnPropChanged("VERSCHIEBCHAR");
            }
        }


        private Visibility abcvisi;

        public Visibility VERSCHIEBVISI
        {
            get { return abcvisi; }
            set
            {
                abcvisi = value;
                OnPropChanged("VERSCHIEBVISI");
            }
        }


        private double _width;

        public double WIDTH
        {
            get { return _width; }
            set
            {
                _width = value;
                OnChange();
            }
        }


        private double size;

        public double SIZE
        {
            get { return size; }
            set
            {
                size = value;
                OnChange();
                OnPropChanged("SIZE");
            }
        }


        private ObservableCollection<Polyline> polylines;

        public ObservableCollection<Polyline> POLYCOLL
        {
            get { return polylines; }
            set { polylines = value; }
        }


        private Visibility textboxvisi;

        public Visibility TEXTBOXVISI
        {
            get { return textboxvisi; }
            set
            {
                textboxvisi = value;
                OnPropChanged("TEXTBOXVISI");
            }
        }


        private Visibility canvisi;

        public Visibility CANVASVISI
        {
            get { return canvisi; }
            set
            {
                canvisi = value;
                OnPropChanged("CANVASVISI");
            }
        }


        private bool? radios1;

        public bool? Radios1
        {
            get { return radios1; }
            set
            {
                radios1 = value;
                OnChange();
            }
        }

        private bool? radios2;

        public bool? Radios2
        {
            get { return radios2; }
            set
            {
                radios2 = value;
                OnChange();
            }
        }

        private bool? radios3;

        public bool? Radios3
        {
            get { return radios3; }
            set
            {
                radios3 = value;
                OnChange();
            }
        }

        private bool? radios4;

        public bool? Radios4
        {
            get { return radios4; }
            set
            {
                radios4 = value;
                OnChange();
            }
        }

        private string instring;

        public string IN
        {
            get { return instring; }
            set
            {
                instring = value;
                OnChange();
            }
        }

        private string outstring;

        public string OUT
        {
            get { return outstring; }
            set
            {
                outstring = value;
                OnPropChanged("OUT");
            }
        }

        private string help;

        public string HELP
        {
            get { return help; }
            set
            {
                help = value;
                OnPropChanged("HELP");
            }
        }

        #endregion


        private void OnChange()
        {
            if (isinitalised)
            {
                if (Radios1 == true)
                {
                    CANVASVISI = Visibility.Collapsed;
                    TEXTBOXVISI = Visibility.Visible;
                    VERSCHIEBVISI = Visibility.Collapsed;
                    MORSEVISI = Visibility.Visible;
                    HELP = "Übersetzbare Zeichen: a-z, ß, äöü, 0-9 mit .,!?.";
                    TextZuMorse();
                }
                else if (Radios2 == true)
                {
                    CANVASVISI = Visibility.Collapsed;
                    TEXTBOXVISI = Visibility.Visible;
                    VERSCHIEBVISI = Visibility.Collapsed;
                    MORSEVISI = Visibility.Visible;
                    HELP = "Übersetzbare Zeichen: a-z, ß, äöü, 0-9 mit .,!? (Punkt: .  Strich: -  Pause: /).";
                    MorseZuText();
                }
                else if (Radios4 == true)
                {
                    CANVASVISI = Visibility.Collapsed;
                    TEXTBOXVISI = Visibility.Visible;
                    MORSEVISI = Visibility.Collapsed;
                    VERSCHIEBVISI = Visibility.Visible;
                    HELP = "übersetzbare Zeichen auswählen";
                    setDrehList();
                    VERSCHIEBINDEX = (int)VERSCHIEB;
                    VERSCHIEBCHAR = drehlist[VERSCHIEBINDEX][0];
                    if (TOGGLECHECK == true)
                    {
                        TOGGLETEXT = "verschieben";
                        VERSCHIEBSTATUS = "Klartext verschieben";
                    }
                    else
                    {
                        TOGGLETEXT = "zurück verschieben";
                        VERSCHIEBSTATUS = "Verschobener Text zu Klartext";
                    }
                    Verschieb();
                }
                else
                {
                    CANVASVISI = Visibility.Visible;
                    TEXTBOXVISI = Visibility.Collapsed;
                    VERSCHIEBVISI = Visibility.Collapsed;
                    MORSEVISI = Visibility.Collapsed;
                    HELP = "Bitte nur Buchstaben (außer äöüß) eingeben!";
                    TextZuKeilschrift();
                }
            }
        }

        public void Verschieb()
        {
            int drehlength = drehlist.Count;
            string strin = IN.ToLower();
            int moveindex = 0;
            string strout = "";

            if (TOGGLECHECK == true)
            {
                moveindex = VERSCHIEBINDEX;
            }
            else
            {
                moveindex = drehlength - VERSCHIEBINDEX;
            }

            int j = strin.Length;
            for (int i = 0; i < j; i++)
            {
                char actual = strin[i];
                int index = drehlist.IndexOf(actual.ToString());
                if (index >= 0)
                {
                    int b = (moveindex + index) % drehlength;

                    strout += drehlist[b];
                }
                else
                {
                    strout += actual;
                }

            }

            OUT = strout;
        }

        public void TextZuMorse()
        {
            bool rememberbackslash = false;
            string strin = IN;
            strin = strin.ToLower();
            string strout = "";

            foreach (char item in strin)
            {
                if (item >= '0' && item <= '9')
                {
                    strout += morsealphabet[item - 18];
                }
                else if (item >= 'a' && item <= 'z')
                {
                    strout += morsealphabet[item - 97];
                }
                else if (item == 'ß')
                {
                    strout += morsealphabet[29];
                }
                else if (item == 'ä')
                {
                    strout += morsealphabet[26];

                }
                else if (item == 'ö')
                {
                    strout += morsealphabet[27];

                }
                else if (item == 'ü')
                {
                    strout += morsealphabet[28];

                }
                else if (item == ',')
                {
                    strout += morsealphabet[43];
                }
                else if (item == '.')
                {
                    strout += morsealphabet[40] + "//";
                }
                else if (item == '!')
                {
                    strout += morsealphabet[41] + "//";
                }
                else if (item == '?')
                {
                    strout += morsealphabet[42] + "//";
                }
                else if (item == ' ')
                {
                    ;//Backslash wird sowieso hinzugefügt
                }
                else if (item == '\n')
                {
                    strout += "/";
                    rememberbackslash = true;
                }
                else if (item == '\r') { continue; }
                else
                {
                    strout += item;
                }
                strout += "/";

                if (rememberbackslash)
                {
                    strout += "\n";
                    rememberbackslash = false;
                }
            }

            OUT = strout;
        }

        public void MorseZuText()
        {
            string strin = IN.Trim().Replace("\n", "").Replace("\r", "");
            var ar = strin.Split('/');
            string strout = "";
            var list = morsealphabet.ToList();

            int j = ar.Length;
            for (int i = 0; i < j; i++)
            {
                string actual = ar[i];

                if (actual != "")
                {
                    int index = list.IndexOf(actual);


                    if (index == -1) //Nicht alphanumerisch
                    {
                        strout += actual;
                    }
                    else
                    {
                        strout += alphabet[index];
                    }

                }
                else
                {
                    if (i != 0)
                    {
                        if (ar[i - 1] == "")
                        {
                            strout += "\n";
                        }
                        else
                        {
                            strout += " ";
                        }
                    }
                }
            }

            OUT = strout;
        }

        public void TextZuKeilschrift()
        {
            GetKeilLines.ResetFalschzeichen();
            double ymerker = 5.0;
            double xmerker = 5.0;
            POLYCOLL = new ObservableCollection<Polyline>();
            int _break = (int)(WIDTH / (SIZE + 5));
            int c = 0;

            string strin = IN.Replace("\r", "").ToLower();

            int j = strin.Length;
            for (int i = 0; i < j; i++)
            {
                if (strin[i] == '\n')
                {
                    ymerker += SIZE + 5;
                    xmerker = 5.0;
                    c = 0;
                    continue;
                }

                if ((c % _break) == 0 && c != 0)
                {
                    ymerker += SIZE + 5;
                    xmerker = 5.0;
                    c = 0;
                }

                c++;
                Polyline dot;
                var line = GetKeilLines.GetIT(new LineRequest(strin[i], xmerker, ymerker, SIZE), out dot);
                xmerker += SIZE + 5;
                if (line is null)
                {
                    continue;
                }
                else
                {
                    POLYCOLL.Add(line);
                }

                if (!(dot is null))
                {
                    POLYCOLL.Add(dot);
                }
            }

            Falschzeichen = GetKeilLines.Falschzeichen ? Visibility.Visible : Visibility.Collapsed;

            OnPropChanged("POLYCOLL");
        }
    }

    public struct LineRequest
    {
        public LineRequest(char _char, double _x, double _y, double _pixelsize)
        {
            CHAR = _char;
            X = _x;
            Y = _y;
            PIXELSIZE = _pixelsize;
        }
        public char CHAR { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double PIXELSIZE { get; set; }
    }

    public static class GetKeilLines
    {
        public static void ResetFalschzeichen() { Falschzeichen = false; }

        private static bool falschzeichen;

        public static bool Falschzeichen
        {
            get { return falschzeichen; }
            private set { falschzeichen = value; }
        }

        private static PointCollection GetPoints(LineRequest lr)
        {
            var pc = new PointCollection();

            double x = lr.X;
            double y = lr.Y;
            double s = lr.PIXELSIZE;

            switch (lr.CHAR)
            {
                case ' ': break;

                case 'a':
                case 'b':
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    pc.Add(new System.Windows.Point(x + s, y));
                    break;

                case 'c':
                case 'd':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    pc.Add(new System.Windows.Point(x + s, y));
                    break;

                case 'e':
                case 'f':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    break;

                case 'g':
                case 'h':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    pc.Add(new System.Windows.Point(x, y + s));
                    break;

                case 'i':
                case 'j':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x, y));
                    break;

                case 'k':
                case 'l':
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    break;

                case 'm':
                case 'n':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    break;

                case 'o':
                case 'p':
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    break;

                case 'q':
                case 'r':
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y));
                    break;


                case 's':
                case 't':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + (s / 2), y + s));
                    pc.Add(new System.Windows.Point(x + s, y));
                    break;

                case 'u':
                case 'v':
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x + (s / 2), y));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    break;

                case 'w':
                case 'x':
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y + (s / 2)));
                    pc.Add(new System.Windows.Point(x, y + s));
                    break;

                case 'y':
                case 'z':
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x, y + (s / 2)));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    break;

                default:
                    double f = s / 5;
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x, y + s));
                    pc.Add(new System.Windows.Point(x, y));
                    pc.Add(new System.Windows.Point(x + s, y));
                    pc.Add(new System.Windows.Point(x + s, y + s));
                    pc.Add(new System.Windows.Point(x + f, y + s));
                    pc.Add(new System.Windows.Point(x + f, y + f));
                    pc.Add(new System.Windows.Point(x + s - f, y + f));
                    pc.Add(new System.Windows.Point(x + s - f, y + s - f));
                    pc.Add(new System.Windows.Point(x + f + f, y + s - f));
                    pc.Add(new System.Windows.Point(x + f + f, y + f + f));
                    pc.Add(new System.Windows.Point(x + s - f - f, y + f + f));
                    pc.Add(new System.Windows.Point(x + s - f - f, y + s - f - f));
                    pc.Add(new System.Windows.Point(x + f + f + f, y + s - f - f));
                    pc.Add(new System.Windows.Point(x + f + f + f, y + f + f + f));
                    Falschzeichen = true;
                    break;
            }

            return pc;
        }

        private static Polyline GetDot(LineRequest lr)
        {
            if (lr.CHAR % 2 == 0 && lr.CHAR > 'a' && lr.CHAR < 'z')
            {
                double mitte0 = (lr.PIXELSIZE / 2) - 0.5;
                double mitte1 = mitte0 + 1;
                var line = GetEmpty();
                var Points = new PointCollection();
                Points.Add(new System.Windows.Point(lr.X + mitte0, lr.Y + mitte0));
                Points.Add(new System.Windows.Point(lr.X + mitte0, lr.Y + mitte1));
                Points.Add(new System.Windows.Point(lr.X + mitte1, lr.Y + mitte1));
                Points.Add(new System.Windows.Point(lr.X + mitte1, lr.Y + mitte0));
                line.Points = Points;
                return line;
            }
            else return null;
        }

        private static Polyline GetEmpty()
        {
            Polyline line = new Polyline();
            line.Stroke = System.Windows.Media.Brushes.Black;
            line.StrokeThickness = 1;

            return line;
        }

        public static Polyline GetIT(LineRequest lr, out Polyline dot)
        {
            dot = GetDot(lr);
            var p = GetPoints(lr);
            if (p is null) return null;
            Polyline line = GetEmpty();
            line.Points = p;
            return line;
        }
    }

    public partial class MainWindow : Window
    {
        mvvm mvvm;
        public MainWindow()
        {
            InitializeComponent();
            mvvm = new mvvm();
            DataContext = mvvm;
            GetIcon();
        }

        public static BitmapSource CaptureScreen(Visual target, double dpiX, double dpiY)
        {
            if (target == null)
            {
                return null;
            }
            Rect bounds = VisualTreeHelper.GetDescendantBounds(target);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                            (int)(bounds.Height * dpiY / 96.0),
                                                            dpiX,
                                                            dpiY,
                                                            PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(target);
                ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
            }
            rtb.Render(dv);
            return rtb;
        }

        private void GetIcon()
        {
            Bitmap icon = Properties.Resources.logo;

            Icon = GetSourceFromBitmap(icon);
        }

        /// <summary>
        /// Zeigt ein Infobild je nach Option
        /// </summary>
        /// <param name="option">false: Keil; true: Morse</param>
        private void showInfoImage(bool? option)
        {
            if (option == false) showInfoWin(GetSourceFromBitmap(Properties.Resources.keil));

            if (option == true) showInfoWin(GetSourceFromBitmap(Properties.Resources.morse));
        }

        private void showInfoWin(BitmapSource source)
        {
            var w = source.Width + 50;
            var h = source.Height + 50;
            Window win = new Window() { Width = w, Height = h, WindowStyle = WindowStyle.ToolWindow };
            var can = new StackPanel();
            can.Children.Add(new System.Windows.Controls.Image() { Source = source });
            win.Content = can;

            win.ShowDialog();
        }

        public static BitmapSource GetSourceFromBitmap(System.Drawing.Bitmap bitmap)
        {
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            var bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                PixelFormatConverter.Convert(bitmap.PixelFormat), null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);

            return bitmapSource;
        }


        private void btcopy_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetImage(CaptureScreen(canvas, 300, 300));
        }

        private void canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            mvvm.WIDTH = canvas.ActualWidth;
        }

        private void btasdf_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Version: 2.3\n© Moritz Schwärzler\nBugreport: moritz.schwaerzler@gmx.net");
        }

        private void btdreh_Click(object sender, RoutedEventArgs e)
        {
            var win = new drehscheibe(ref mvvm);
            win.ShowDialog();
        }

        private void btcopytext_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(mvvm.OUT);
        }

        private void btinfokeil_Click(object sender, RoutedEventArgs e)
        {
            showInfoImage(false);
        }

        private void btinfomorse_Click(object sender, RoutedEventArgs e)
        {
            showInfoImage(true);
        }
    }


    public static class PixelFormatConverter
    {
        public static System.Windows.Media.PixelFormat Convert(this System.Drawing.Imaging.PixelFormat pixelFormat)
        {
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppGrayScale)
                return System.Windows.Media.PixelFormats.Gray16;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppRgb555)
                return System.Windows.Media.PixelFormats.Bgr555;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppRgb565)
                return System.Windows.Media.PixelFormats.Bgr565;

            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Indexed)
                return System.Windows.Media.PixelFormats.Bgr101010;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed)
                return System.Windows.Media.PixelFormats.Indexed1;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format4bppIndexed)
                return System.Windows.Media.PixelFormats.Indexed4;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
                return System.Windows.Media.PixelFormats.Indexed8;

            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
                return System.Windows.Media.PixelFormats.Bgr24;

            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb)
                return System.Windows.Media.PixelFormats.Bgr32;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppPArgb)
                return System.Windows.Media.PixelFormats.Pbgra32;
            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppRgb)
                return System.Windows.Media.PixelFormats.Bgr32;

            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format48bppRgb)
                return System.Windows.Media.PixelFormats.Rgb48;

            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Format64bppArgb)
                return System.Windows.Media.PixelFormats.Prgba64;

            if (pixelFormat == System.Drawing.Imaging.PixelFormat.Undefined)
                return System.Windows.Media.PixelFormats.Default;

            throw new NotSupportedException("Convertion not supported with " + pixelFormat.ToString());
        }

        public static System.Drawing.Imaging.PixelFormat Convert(this System.Windows.Media.PixelFormat pixelFormat)
        {
            if (pixelFormat == System.Windows.Media.PixelFormats.Gray16)
                return System.Drawing.Imaging.PixelFormat.Format16bppGrayScale;
            if (pixelFormat == System.Windows.Media.PixelFormats.Bgr555)
                return System.Drawing.Imaging.PixelFormat.Format16bppRgb555;
            if (pixelFormat == System.Windows.Media.PixelFormats.Bgr565)
                return System.Drawing.Imaging.PixelFormat.Format16bppRgb565;

            if (pixelFormat == System.Windows.Media.PixelFormats.Bgr101010)
                return System.Drawing.Imaging.PixelFormat.Indexed;
            if (pixelFormat == System.Windows.Media.PixelFormats.Indexed1)
                return System.Drawing.Imaging.PixelFormat.Format1bppIndexed;
            if (pixelFormat == System.Windows.Media.PixelFormats.Indexed4)
                return System.Drawing.Imaging.PixelFormat.Format4bppIndexed;
            if (pixelFormat == System.Windows.Media.PixelFormats.Indexed8)
                return System.Drawing.Imaging.PixelFormat.Format8bppIndexed;

            if (pixelFormat == System.Windows.Media.PixelFormats.Bgr24)
                return System.Drawing.Imaging.PixelFormat.Format24bppRgb;

            if (pixelFormat == System.Windows.Media.PixelFormats.Bgr32)
                return System.Drawing.Imaging.PixelFormat.Format32bppArgb;
            if (pixelFormat == System.Windows.Media.PixelFormats.Pbgra32)
                return System.Drawing.Imaging.PixelFormat.Format32bppPArgb;
            if (pixelFormat == System.Windows.Media.PixelFormats.Bgr32)
                return System.Drawing.Imaging.PixelFormat.Format32bppRgb;

            if (pixelFormat == System.Windows.Media.PixelFormats.Rgb48)
                return System.Drawing.Imaging.PixelFormat.Format48bppRgb;

            if (pixelFormat == System.Windows.Media.PixelFormats.Prgba64)
                return System.Drawing.Imaging.PixelFormat.Format64bppArgb;

            if (pixelFormat == System.Windows.Media.PixelFormats.Default)
                return System.Drawing.Imaging.PixelFormat.Undefined;

            throw new NotSupportedException("Convertion not supported with " + pixelFormat.ToString());
        }
    }
}