# Geheimschriftenübersetzer

Tool, mit dem Geheimschriften übersetzt werden können:
- Morsecode in Klartext und zurück verwandeln
- Klartext zur Keilschrift
- Buchstabenverschiebung

Dies wurde gemacht, um bei Pfaditruppstunden die Vorbereitung zu erleichtern.


## Text zu Morse

![image](pics/morsen.png)

Der eingegebene Text wird sofort in den entsprechenden Morsecode übersetzt.
Nicht übersetzbare Zeichen werden ungeändert eingefügt.


## Morse zu Text

![image](pics/morsezutext.png)

Der eingegebene Morsecode wird beim Schreiben in Klartext konvertiert.
Nicht übersetzbare Zeichen werden ungeändert eingefügt.


## Text zu Keilschrift

![image](pics/keil.png)

Jede Rune repräsentiert einen Buchstaben, welcher beim Eingeben gezeichnet wird.
Bei nicht übersetztbaren Zeichen wird ein Platzhalter eingefügt.


## Buchstabenverschieb

![image](pics/verschieb.png)

Mit dem Slider wird ausgewählt, um wie viele Zeichen verschoben werden soll. Zudem kann ausgewählt werden, dass äöüß und oder Zahlen mit einbezogen werden.
Nicht übersetzbare Zeichen werden ungeändert eingefügt.


## Vorschau auf Drehscheibe

![image](pics/drehscheibe.png)

Um die Reihenfolge der Zeichen und die Verschiebung anzuzeigen, kann ein Fenster mit einer Kurzansicht geöffnet werden.


## Codemap

![image](pics/codemap.png)
